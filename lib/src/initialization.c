#include "../includes/initialization.h"

// Uses globals: 
// MAZE, MAZE_DIM_X, MAZE_DIM_Y
bool initialize_maze(int inital_value)
{
    int x = 0;
    int y = 0;
   
    for(x; x < MAZE_DIM_X; x++)
        for(y; y < MAZE_DIM_Y; y++)
        {
            MAZE[x][y] = inital_value;
            if(MAZE[x][y] != inital_value)
                return false;
        }
    return true;
}
