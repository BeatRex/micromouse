#ifndef GLOBALS

#define GLOBALS
//Global macros

//Maze dimensions per square
#define MAZE_DIM_X 16
#define MAZE_DIM_Y 16

#ifdef GLOBAL_FILE
//If the file is src/globals.c 

//Globals for motor physics
// Using ints as a place holder since they may be floats
// If they remain ints we should switch these to macros
int RADIUS;
int CIRCUMFERENCE;
int PERIOD;

//Flood fill globals
int MAZE[MAZE_DIM_Y][MAZE_DIM_X]

#else
// If the file is not src/globals.c
extern int RADIUS;
extern int CIRCUMFERENCE;
extern int PERIOD;
extern int MAZE[MAZE_DIM_Y][MAZE_DIM_X]
#endif
#endif
