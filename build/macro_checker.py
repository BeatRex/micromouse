import re
import sys

f = open('../lib/src/globals.c', 'r')
f = f.read()
find = re.findall(r'(NEEDS_TO_BE_DEFINED)', f)

if len(find) != 0:
    print "ERROR: Some macros are not defined"
    sys.exit(1)
else:
    sys.exit(0)
